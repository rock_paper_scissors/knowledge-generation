from import_packages import *
from parameter_declaration import *


def generate_qualitative_values(property_data, cluster_algorithm):


      if cluster_algorithm == 'c':
            fpcs = []

            cntr, labels, u0, d, jm, p, fpc = fuzz.cluster.cmeans(property_data, number_of_clusters, 2, error=0.005, maxiter=1000, init=None)

      elif cluster_algorithm == 'k':
            print np.shape(property_data)
            kmeans = KMeans(n_clusters=number_of_clusters, random_state=0).fit(property_data)
            labels = kmeans.labels_



      # u contains the soft clustered data
      return(labels)


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


def generate_qualitative_instance_knowledge(property_name, objs, property_data, cluster_algorithm):


      # generate qualitative values by fuzzy clustering the property data
      clustered = generate_qualitative_values(property_data, cluster_algorithm)

      # if the instances are categorized using fuzzy c means

      if cluster_algorithm == 'c':

            # check whether instance knowledge has already been activated by checking if the instance knowledge of any kind
            # of object exists
            if os.path.isfile(instance_knowledge_file):
                  print "Instance knowledge base file exists..."
                  with open(instance_knowledge_file) as instance_json:
                        # extract the existing instance knowledge
                        old_instance_knowledge_file = json.load(instance_json)
                  initiate_trigger = 0
            else:
                  print "Instance knowledge base file does not exist..."
                  old_instance_knowledge_file = {}
                  initiate_trigger = 1

            new_instance_knowledge_file = {}

            # number of rows = number of clusters, while number of columns = number of data points or instances
            row, column = np.shape(clustered)


            # iterate through each cluster
            for r in range(row):

                  # create symbolic labels for the clusters with respect to the property name and number of clusters
                  property_quality_measure = property_name + "_" + str(r)

                  # iterate through each data point in the given cluster
                  for c in range(column):

                        # generate a membership value between the range [0, 100]
                        fuzzy_value = round(clustered[r][c] * 100, 2)

                        # check if the object instance already exists in the existing instance knowledge
                        # if exists, update the existing instance knowledge
                        if objs[c] in old_instance_knowledge_file:
                              if property_quality_measure not in old_instance_knowledge_file[objs[c]]:
                                    old_instance_knowledge_file[objs[c]].update({property_quality_measure:fuzzy_value})
                              #else:
                              #      print 'Property instance knowledge exists...'

                        # if it does not exist, add the object instance in the new instance knowledge.
                        else:
                              if objs[c] in new_instance_knowledge_file:
                                    if property_quality_measure not in new_instance_knowledge_file[objs[c]]:
                                          new_instance_knowledge_file[objs[c]].update({property_quality_measure: fuzzy_value})
                              else:
                                    new_instance_knowledge_file.update({objs[c]:{property_quality_measure:fuzzy_value}})


            if len(new_instance_knowledge_file.keys()) != 0:
                  old_instance_knowledge_file.update(new_instance_knowledge_file)
            else:
                  print "No new knowledge has been created..."

            with open(instance_knowledge_file, 'w') as new_instance_json:
                  json.dump(old_instance_knowledge_file, new_instance_json, indent=4)
            if initiate_trigger == 0:
                  print "Instance knowledge base updated..."
            elif initiate_trigger == 1:
                  print "Instance knowledge base is initiated..."

      # if the instances are categorized using k means

      elif cluster_algorithm == 'k':

            labels_length = len(clustered)
            #print 'label length is' + str(labels_length)

            instance_quality_labels = []

            for label in clustered:
                  # create symbolic labels for the clusters with respect to the property name and number of clusters
                  instance_quality_labels.append(property_name + '_' + str(label))

            if os.path.isfile(instance_knowledge_file):
                  print "Instance knowledge base file exists..."
                  with open(instance_knowledge_file, "r") as exists:
                        existing_instance_knowledge = json.load(exists)
            else:
                  existing_instance_knowledge = {i: [] for i in objs}

            # instance_property_labels_dict = {cup_1:[], cup_2:[], cup_3:[],...}
            instance_property_labels_dict = {i: [] for i in objs}


            for l in range(labels_length):
                  if objs[l] not in existing_instance_knowledge:
                        existing_instance_knowledge.update({objs[l]: []})
                  if instance_quality_labels[l] not in existing_instance_knowledge[objs[l]]:
                        existing_instance_knowledge[objs[l]].append(instance_quality_labels[l])


            with open(instance_knowledge_file, "w") as f1:
                  json.dump(existing_instance_knowledge, f1, indent=4)



      return

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


def create_object_concept(obj_class_info, instance_knowledge, cluster_algorithm):

      # check if the file tracking known object classes, instances and their properties

      if os.path.isfile(known_obj_instances_track_file):
            print 'Known classes, instances and properties is being tracked, already...'
            with open(known_obj_instances_track_file) as known_read:
                  known_instance_knowledge = json.load(known_read)
      else:
            print 'Tracking of known classes, instances and properties is activated...'
            known_instance_knowledge = {}


      if os.path.isfile(obj_concept_knowledge_file):
            print 'Object concept knowlede file exists...'
            with open(obj_concept_knowledge_file) as concept_read:
                  existing_concept_knowledge = json.load(concept_read)
      else:
            print 'Object concept knowledge file does not exist, creating one...'
            existing_concept_knowledge = {}


      # extract object class from obj_class_info
      for cls in obj_class_info:
            # check if the object category exist in the knowledge base
            # if it does not exist, then add the category to the knowledge base as well as in the catelog of known categories
            # and their instances
            if cls not in existing_concept_knowledge:
                  existing_concept_knowledge.update({cls:{}})
                  known_instance_knowledge.update({cls:{}})
            # assuming the object category exist in the knowledge base, check whether the instance and its corresponding
            # knowledge about the properties exist
            for inst in obj_class_info[cls]:
                  # extract the knowledge about the instance from the instance knowledge dictionary
                  single_instance_knowledge = instance_knowledge[inst]
                  # check whether the instance is known or unknown
                  if inst not in known_instance_knowledge[cls]:
                        # add the instance to the list of known instances
                        known_instance_knowledge[cls].update({inst:[]})
                  for prop in single_instance_knowledge:
                        # check if the property is listed as the known property knowledge for the given instance
                        if prop not in existing_concept_knowledge[cls]:
                              # add the property knowledge of the given instance to the concept knowledge
                              # process is different for k - means and for fuzzy c means clustered instances
                              if cluster_algorithm == 'c':
                                    existing_concept_knowledge[cls].update({prop:single_instance_knowledge[prop]})
                              elif cluster_algorithm == 'k':
                                    existing_concept_knowledge[cls].update({prop:1})
                              # add the property to the known instance knowledge
                              known_instance_knowledge[cls][inst].append(prop)
                        elif prop in existing_concept_knowledge[cls]:
                              # check if the property is in the known instance knowledge
                              if prop not in known_instance_knowledge[cls][inst]:
                                    # add the property to the known instance knowledge
                                    known_instance_knowledge[cls][inst].append(prop)
                                    # update the knowledge about the property for the object concept knowledge
                                    old_val = existing_concept_knowledge[cls][prop]
                                    if cluster_algorithm == 'c':
                                          existing_concept_knowledge[cls][prop] = old_val + instance_knowledge[inst][prop]
                                    elif cluster_algorithm == 'k':
                                          existing_concept_knowledge[cls][prop] = old_val + 1

            # number of instances in the object class
            number_of_instances = len(obj_class_info[cls])

            for prop in existing_concept_knowledge[cls]:
            ############# TODO - the if condition is confusing, please revise it.
                  # skip the key containing known instance knowledge info
                  if prop != known_instance_knowledge:
                        prop_value = float(existing_concept_knowledge[cls][prop])
                        fnumber_of_instances = float(number_of_instances)
                        # compute the average value of the property of the
                        average_prop_value = round(prop_value/fnumber_of_instances, 2)
                        existing_concept_knowledge[cls][prop] = average_prop_value



      with open(obj_concept_knowledge_file, 'w') as concept_write:
            json.dump(existing_concept_knowledge, concept_write, indent=4)
            print 'Concept knowledge has been updated...'

      with open(known_obj_instances_track_file, 'w') as known_write:
            json.dump(known_instance_knowledge, known_write, indent=4)
            print 'Known classes, instances and their properties has been updated...'


      return

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


def create_function_model(existing_instance_knowledge, cluster_algorithm):

      if cluster_algorithm == 'k':
            # check whether the function model file exist
            if os.path.isfile(function_model_file):
                  print 'Function model file exists...'
                  exit()
#                  with open(function_model_file) as function_read:
#                        existing_function_model = json.load(function_read)
            else:
                  print 'Function model file does not exist, creating one...'
                  existing_function_model = {}


            function_property_total_presence = {}

            # create a function model
            # iterate through every instance knowledge
            for inst in existing_instance_knowledge:

                  # separate physical properties and functional properties in the given instance knowledge
                  f_prop = []
                  p_prop = []

                  for k in existing_instance_knowledge[inst]:
                        if k in functional_properties:
                              f_prop.append(k)
                        elif k in physical_properties:
                              p_prop.append(k)

                  #print f_prop
                  #print p_prop
                  # iterate through list of functional properties
                  for f in f_prop:
                        # compute the total number time a functional property is present in the knowledge base
                        if f not in function_property_total_presence:
                              function_property_total_presence.update({f:1})
                        elif f in function_property_total_presence:
                              a = function_property_total_presence[f]
                              function_property_total_presence[f] = a + 1

                        if f not in existing_function_model:
                              existing_function_model.update({f:{}})
                        for p in p_prop:
                              if p not in existing_function_model[f]:
                                    existing_function_model[f].update({p:1})
                              elif p in existing_function_model[f]:
                                    b = existing_function_model[f][p]
                                    existing_function_model[f][p] = b + 1

            #print existing_function_model['support_1']["rigidity_1"]
            #print function_property_total_presence['support_1']

            # calculate the average association between a physical property and a functional property
            for function in existing_function_model:
                  for physical in existing_function_model[function]:
                        old_val = existing_function_model[function][physical]
                        #print "old " + str(old_val)
                        existing_function_model[function][physical] = round(float(old_val) / float(function_property_total_presence[function]), 3)



      with open(function_model_file, 'w') as function_write:
            json.dump(existing_function_model, function_write, indent=4)

      return


                                    #existing_property_membership = create_property_membership_compile(instance_knowledge)

'''
      ################# IMPORTANT: DO NOT DELETE THE BELOW CODE #####################################
      ############## old c mean code
      # iterate over each functional property
      for func in functional_properties:
            if func not in existing_function_model:
                  existing_function_model.update({func:{}})
            # extract the membership values of the functional property
            functional = existing_property_membership[func]
            # iterate over all the physical properties
            for prop in existing_property_membership:
                  # check whether the property is physical
                  if prop not in functional_properties:
                        # extract the membership values of the physical property
                        physical = existing_property_membership[prop]
                        if len(functional) == len(physical):
                              # calculate the pearson's correlation coefficient between
                              # the functional property and physical property
                              coefficient, v = pearsonr(functional, physical)
                              coefficient = round(coefficient, 3)
                              # add the coefficient value for the physical property in the function model
                              existing_function_model[func].update({prop:coefficient})
'''



# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


def create_property_membership_compile(instance_knowledge):


      '''
      # check whether the instance property membership compile file exist
      if os.path.isfile(property_membership_compile_file):
            existing_property_membership = {}
            print 'Instance property membership compile file exists...'
            # reads the csv file column wise
            function_reader = csv.DictReader(open(property_membership_compile_file))
            for row in function_reader:
                  for column, value in row.iteritems():
                        existing_property_membership.setdefault(column, []).append(value)
      else:
            print 'Instance property membership compile file does not exist, creating one...'
            existing_property_membership = {}

      '''
      existing_property_membership = {}

      # iterate through each instance and compile all the membership values for each property
      for inst in instance_knowledge:
            properties = instance_knowledge[inst]
            for prop in properties:
                  if prop not in existing_property_membership:
                        existing_property_membership.update({prop:[properties[prop]]})
                  else:
                        existing_property_membership[prop].append(properties[prop])


      return existing_property_membership


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++











