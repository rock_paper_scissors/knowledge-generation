from import_packages import *
from extract_property_data_from_csv import *
from create_conceptual_knowledge import *



def activate_knowledge_base(trigger_name, cluster_algorithm):
    if trigger_name == 'instance':
        activate_instance_knowledge(cluster_algorithm)
    elif trigger_name == 'concept':
        activate_concept_knowledge(cluster_algorithm)
    elif trigger_name == 'function_model':
        activate_function_model(cluster_algorithm)
    elif trigger_name == 'none':
        print 'Knowledge base is dormant...'

    return


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++





def activate_instance_knowledge(cluster_algorithm):


    # scan each property file for cluster processing
    print property_files_list
    for file in property_files_list:

        print 'Processing -----> ' + str(file)
        # extract the property data
        property_file = property_files_path + file
        instance_list, property_data = extract_property_data(property_file)

        # separate the property name from the file
        file_parts = file.split('.')
        property_name = file_parts[0]
        #print property_name
        #plot_property_data_distribution(property_name, instance_list, property_data)

        # create instance knowledge about the property
        generate_qualitative_instance_knowledge(property_name, instance_list, property_data, cluster_algorithm)
        print file + ' has been processed...'

    print 'Instance knowledge is processed....'

    return


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


def activate_concept_knowledge(cluster_algorithm):

    if os.path.isfile(obj_class_file):
        with open(obj_class_file) as class_read:
            obj_class_info = json.load(class_read)
    else:
        print 'Object class file does not exist...'

    if os.path.isfile(instance_knowledge_file):
        with open(instance_knowledge_file) as instance_read:
            instance_knowledge = json.load(instance_read)

    create_object_concept(obj_class_info, instance_knowledge, cluster_algorithm)

    return

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


def activate_function_model(cluster_algorithm):
    if os.path.isfile(instance_knowledge_file):
        with open(instance_knowledge_file) as instance_read:
            instance_knowledge = json.load(instance_read)

    create_function_model(instance_knowledge, cluster_algorithm)

    return


#######################################################################################################################



activate_knowledge_base('instance', 'k')
activate_knowledge_base('concept', 'k')
activate_knowledge_base('function_model', 'k')




# *********************************************************************************************************************#
# ****** CHECKLIST FOR ROCS DATASET *******#
# 1) OPEN FILE: extract_property_data_from_csv.py
# 2) LOCATE FUNCTION DEFINITION OF A FUNCTION: extract_property_data
# 3) LOCATE A LINE: next(readCSV)
# 4) UNCOMMENT THE LINE IF IT IS COMMENTED
# *********************************************************************************************************************#

# *********************************************************************************************************************#
# ****** CHECKLIST FOR WASHINGTON DATASET *******#
# 1) OPEN FILE: extract_property_data_from_csv.py
# 2) LOCATE FUNCTION DEFINITION OF A FUNCTION: extract_property_data
# 3) LOCATE A LINE: next(readCSV)
# 4) COMMENT THE LINE IF IT IS UNCOMMENTED
# *********************************************************************************************************************#





