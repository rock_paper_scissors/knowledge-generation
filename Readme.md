#### Background Information
The <mark>Knowledge Base</mark> contains the knowledge about physical and functional properties of objects. The conceptual knowledge about objects in our research work is characterized as *qualitative*, *generalized*, *relative*, and *subjective*. In the following, we provide the proposed interpretation of the aforementioned characterizations of the conceptual knowledge.  
1. **Generalized:** The conceptual knowledge about objects primarily consists of knowledge about an object class as opposed to a specific instance
of the object class. However, it is derived from the instances of the object class.
2. **Relative:** The general knowledge about any object class is based on its instances that have been encountered. It means, the knowledge about any object class is not absolute and is subject to change as more instances of the object class are encountered.
3. **Subjective:** The knowledge about an object class is acquired from the sensory experiences and interaction with the object’s instances as opposed to extracted from other sources. In our research work, the subjective knowledge is knowledge acquired on the basis of a robot’s sensory experiences and interaction with objects. The subjective knowledge is referred as robot-centric knowledge in our work. 
4. **Qualitative:** The knowledge about object classes is expressed in terms of the properties where the properties observed in the objects are interpreted qualitatively as opposed to quantitatively. However, the qualitative knowledge about properties of objects is obtained from the quantitative data about properties of objects.  
In order to generate such conceptual knowledge, we have proposed a bottom-up method which consists of five levels of abstractions as illustrated in figure below. The bottom three levels, which are covered in git repository: <mark>[Property Estimation](https://gitlab.com/rock_paper_scissors/property_estimation)</mark> project, focus on gathering the sensory data and estimating the properties measurements on the basis of the sensory data. For each property, we have uploaded the estimated measurements in the git repository: <mark>[Dataset](https://gitlab.com/rock_paper_scissors/dataset)</mark> project. The top two levels, on the other hand, concentrate on generating the conceptual knowledge about objects from the property easurements. For generating robot-centric conceptual knowledge, the data about the objects’ physical and functional properties is processed in two stages: sub-categorization (Layer 4) and conceptualization (Layer 4 and 5). For our proposed knowledge base, we have used the symbolic formalism based on the notion of fuzzy sets and attribute-value pairs as suitable
formalism. The knowledge base is provided with a set of symbols which form the vocabulary using which the proposed knowledge base is built.

![Knowledge Layers](https://drive.google.com/uc?export=view&id=1tmfVMLDiboRfao-UjJPKz4NqCdeIz4yv 'Knowledge Generation Steps')

The figure below illustrates the conceptual knowledge generation process is illustrated where acquired continuous property data of objects \{o<sub>1</sub> , o<sub>2</sub> ...\} is subcategorized into multiple clusters. Using Bi-variate joint frequency distribution and sample proportions, conceptual knowledge about object classes (e.g. plastic_box) is generated.

![Knowledge Generation](https://drive.google.com/uc?export=view&id=1-cYcut8f7wANmstfJ7x7K4u8-KBbGglm 'Knowledge Generation Process')

##### Sub-categorization
The sub-categorization process is the first step in creating conceptual knowledge about object classes. The process generates (more intuitive) qualitative measures, to represent the degree with which a property (physical or functional) is reflected in an object instance, unsupervisedly using a clustering mechanism on the quantitative measurements of a property estimated in the object instances. In that, a cluster of the property measurements can be seen as a qualitative measure of the corresponding property.

##### Conceptualization
The conceptualization process is twofold: in the first step, it generates the knowledge about all the instances and in the second step, it generates knowledge about object classes on the basis of the knowledge about the instances. As illustrated in the top two levels in figure above, the resulting conceptual knowledge about objects consist of two layers: knowledge about object instances and knowledge about object classes. 

###### Knowledge about object instances
In order to generate knowledge about each object instance, we aggregate all the physical and functional quality labels assigned to physical and functional property estimation values of each object instance in the sub-categorization step. The knowledge about object instances is a compilation of knowledge of each object instance.

###### Knowledge about object classes
The second stage of the conceptualization process generates the knowledge about an object class by aggregating knowledge about its instances. The underlying principle is to represent an object class in terms of the overall observations made about its instances which encompasses the frequently observed properties as well as less frequently observed properties associated with it. In order to represent the frequency of qualitative measures of the physical or functional
properties observed in an object class, we employed a statistical technique called bivariate joint frequency distribution. While the frequency offers the number of times a certain quality is observed in the encountered instances of an object class, it does not state the share of the quality within the encountered instances of the object class. In order to determine the share of the quality, sample proportion is calculated by using the following formula:

![Sample Proportion](https://drive.google.com/uc?export=view&id=1rkdQUlAX6b_6Bxkm9RgnI6VpwR8KJYky 'Sample Proportion Formula')

###### Function Models
In addition to conceptual knowledge about objects, the conceptualization process also creates knowledge about functional qualities (qualitative measures), termed as function models, by associating the occurrence of physical qualities in an object instance given the occurrence of a functional quality in the instance and aggregating the result of such conditional occurrences. The idea behind the function model is to identify the physical qualities which are correlated with functional qualities. The functional model generation follows the similar two-step process as the conceptual knowledge generation: first, the bivariate joint frequency distribution is applied which is then followed by sample proportion calculation. Note that, in order to generate a function model for each functional quality, the knowledge about all instances are considered.

### Structure

Python version - 2.7

##### Python files:

1. <mark>activate_knowledge_base.py</mark> - activates the knowledge generation process

2. <mark>create_conceptual_knowledge.py</mark> - creates object instance knowledge, object class knowledge and function model

3. <mark>extract_property_data_from_csv.py</mark> - extracts the property measurements for each property 

4. <mark>import_packages.py</mark> - imports the required packages

5. <mark>parameter_declaration.py</mark> - contains the information such as number of clusters, file paths etc.

6. <mark>requirements.txt</mark> - contains the list of all dependencies. Run the following command to install all the dependencies:
```
pip install -r requirements.txt
```
---

##### Directories:

1. <mark>property_data/</mark> : contains csv files of property measurements of six physical and four functional properties using property estimation framework called RoCS  

2. <mark>Knowledge_Base/</mark> : stores the knowledge files such as object instance knowledge, object class knowledge and function model

---

##### How to generate knowledge - Instruction:

1. Open the file <mark>activate_knowledge_base.py</mark>  

2. Locate the lines: 
<mark>activate_knowledge_base('instance', 'k')</mark>,
<mark>activate_knowledge_base('concept', 'k')</mark>,
<mark>activate_knowledge_base('function_model', 'k')</mark>  

3. Ensure that all the lines are uncommented  

4. Run the file by typing the following command on a command line: 
```
python activate_knowledge_base.py
```
