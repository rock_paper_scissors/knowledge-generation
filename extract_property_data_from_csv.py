from import_packages import *
from parameter_declaration import *

def extract_property_data(property_file):

    instance_name = []
    property_data = []

    with open(property_file) as csvfile:

        readCSV = csv.reader(csvfile, delimiter = ",")

        # ignore the headers: instance_name, feature_1, feature_2 etc.
        # comment it our if there are no headers such as instance name, feature_1 etc.
        next(readCSV)

        # assuming that the property data is one dimensional (scalar value)
        for row in readCSV:
            name = row[0]
            data = row[1:]

            instance_name.append(name)
            property_data.append(data)


    # convert the string values of the arrays into floating numbers
    property_data = np.array(property_data).astype(np.float)

    extract_class_names(instance_name)

    return instance_name, property_data





def extract_class_names(instance_name_list):

    if os.path.isfile(obj_class_file):
        print 'Object class info file exists...'
        with open(obj_class_file) as obj_class_json:
            class_instance_list = json.load(obj_class_json)
    else:
        class_instance_list = {}

    print 'inside extract class names'
    print 'file name is ' + obj_class_file

    for instance in instance_name_list:
        # separate the integers from the rest of the string
        instance_split = filter(None, re.split(r'(\d+)', instance))
        # instance_split = ['cup_', '1']
        obj_class = instance_split[0]
        if obj_class in class_instance_list:
            if instance not in class_instance_list[obj_class]:
                class_instance_list[obj_class].append(instance)
        elif obj_class not in class_instance_list:
            class_instance_list.update({obj_class:[instance]})

    with open(obj_class_file, 'w') as obj_class_write:
        json.dump(class_instance_list, obj_class_write, indent=4)


# *********************************************************************************************************************#
# ****** CHECKLIST FOR ROCS DATASET *******#
# 1) OPEN FILE: extract_property_data_from_csv.py
# 2) LOCATE FUNCTION DEFINITION OF A FUNCTION: extract_property_data
# 3) LOCATE A LINE: next(readCSV)
# 4) UNCOMMENT THE LINE IF IT IS COMMENTED
# *********************************************************************************************************************#

# *********************************************************************************************************************#
# ****** CHECKLIST FOR WASHINGTON DATASET *******#
# 1) OPEN FILE: extract_property_data_from_csv.py
# 2) LOCATE FUNCTION DEFINITION OF A FUNCTION: extract_property_data
# 3) LOCATE A LINE: next(readCSV)
# 4) COMMENT THE LINE IF IT IS UNCOMMENTED
# *********************************************************************************************************************#


